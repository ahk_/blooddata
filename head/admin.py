from django.contrib import admin


from .models import Doner, ContactNumber
# Register your models here.


class ContactNumberInline(admin.StackedInline):
    model = ContactNumber
    extra = 1


class DonerAdmin(admin.ModelAdmin):
    inlines = [ContactNumberInline]
    class Meta:
        model = Doner

admin.site.register(Doner, DonerAdmin)
admin.site.register(ContactNumber)

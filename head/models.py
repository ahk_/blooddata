import datetime

from django.db import models



# Create your models here.


class Doner(models.Model):
    name = models.CharField(max_length=500)
    blood_group = models.CharField(max_length=7)


    def __str__(self):
        return self.name


class ContactNumber(models.Model):
    number = models.IntegerField()
    doner = models.ForeignKey(Doner)

    def __str__(self):
        return "{0}'s number is {1}".format(self.doner.name, self.number)

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('head', '0002_contactnumber'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='doner',
            name='contact_number',
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('head', '0006_remove_contactnumber_source'),
    ]

    operations = [
        migrations.AddField(
            model_name='contactnumber',
            name='source',
            field=models.CharField(default='', max_length=10),
            preserve_default=False,
        ),
    ]

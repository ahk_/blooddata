# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('head', '0004_auto_20150417_1248'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contactnumber',
            name='source',
            field=models.ForeignKey(blank=True, to='head.Organization', null=True),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('head', '0009_auto_20150417_1709'),
    ]

    operations = [
        migrations.AddField(
            model_name='org',
            name='color',
            field=models.CharField(default='#002366', max_length=10),
            preserve_default=False,
        ),
    ]

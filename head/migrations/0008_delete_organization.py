# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('head', '0007_contactnumber_source'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Organization',
        ),
    ]

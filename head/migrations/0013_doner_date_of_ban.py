# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('head', '0012_doner_available'),
    ]

    operations = [
        migrations.AddField(
            model_name='doner',
            name='date_of_ban',
            field=models.DateField(default=datetime.date.today),
        ),
    ]

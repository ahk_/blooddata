# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('head', '0010_org_color'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contactnumber',
            name='source',
        ),
        migrations.DeleteModel(
            name='Org',
        ),
    ]

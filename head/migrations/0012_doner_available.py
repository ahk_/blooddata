# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('head', '0011_auto_20150420_0542'),
    ]

    operations = [
        migrations.AddField(
            model_name='doner',
            name='available',
            field=models.BooleanField(default=True),
        ),
    ]

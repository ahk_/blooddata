# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('head', '0003_remove_doner_contact_number'),
    ]

    operations = [
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=500)),
                ('color', models.CharField(max_length=10)),
            ],
        ),
        migrations.AddField(
            model_name='contactnumber',
            name='source',
            field=models.ForeignKey(default=None, to='head.Organization'),
            preserve_default=False,
        ),
    ]

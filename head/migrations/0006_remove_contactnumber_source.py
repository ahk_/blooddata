# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('head', '0005_auto_20150417_1249'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contactnumber',
            name='source',
        ),
    ]

from django.shortcuts import render
from django.http import HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .models import ContactNumber, Doner
# Create your views here.

import random

BLOOD_TUPLE = (
        ('ap', 'A +ve'),
        ('an','A -ve'),
        ('abp', 'AB +ve'),
        ('abn', 'AB -ve'),
        ('bp', 'B +ve'),
        ('bn', 'B -ve'),
        ('op', 'O +ve'),
        ('on', 'O -ve')
        )

def home(request):
    page = request.GET.get("page", "1")
    search = dict(BLOOD_TUPLE).get(request.GET.get("bloodSearch"), "")

    allcontacts = list(ContactNumber.objects.all())
    random.shuffle(allcontacts)

    if search == '':
        contactlist = allcontacts
    else:
        # search string is available
        contactlist = []
        for contact in allcontacts:
            if search.lower() == contact.doner.blood_group.lower():
                contactlist.append(contact)

    pagitated_contact_list = Paginator(contactlist, 10)
    
    try:
        contacts = pagitated_contact_list.page(page)
    except PageNotAnInteger:
        # If page is not an integer
        contacts = pagitated_contact_list.page(1)
    except EmptyPage:
        # If page out of range
        contacts = pagitated_contact_list.page(pagitated_contact_list.num_pages)


    chunk_size = int(len(contacts) / 3)

    if chunk_size <= 0:chunk_size = 1
    
    contactlists = [contacts[l:l+chunk_size] 
            for l in range(0 , len(contacts), chunk_size)]

    pages = [(n, n == contacts.number) 
                for n in range(contacts.number - 5,contacts.number + 5 ) 
                    if n > 0 and n < pagitated_contact_list.num_pages]
    
    return render(request,
                    'head/home.html',
                    {'contactlists': contactlists,
                        'contacts': contacts,
                        'pages': pages,
                        'max_page': pagitated_contact_list.num_pages-1,
                        'blood_list': BLOOD_TUPLE,
                        'bloodSearch': {k: v for v,k in dict(BLOOD_TUPLE).items()},
                        'bloofSearchFlip': dict(BLOOD_TUPLE),
                        'current_blood_filter': search,
                        'raw_search_string':request.GET.get("bloodSearch"),
                    }
                )
